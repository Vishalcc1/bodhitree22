const { defineConfig } = require("cypress");
// cy.viewport(550, 750)
module.exports = defineConfig({
  viewportHeight:860,
  viewportWidth:1500,
  "reporter": "cypress-mochawesome-reporter",
  "reporterOptions": {
    "reportDir": "cypress/reports",
    "charts": true,
    "reportPageTitle": "My Test Suite",
    "embeddedScreenshots": true,
    "inlineAssets": true
  },
  "video": false,
  // video: false,
//screenshotOnRunFailure:true,  
//video:true,  
// videoCompression: 32,
// videoUploadOnPasses: true,
  defaultCommandTimeout: 20000000,
  pageLoadTimeout: 20000000,
  e2e: {
    baseUrl: 'https://mayur.bodhi.cse.iitb.ac.in/login',
    setupNodeEvents(on, config) {
      // implement node event listeners here
      return require('./cypress/plugins/index.js')(on, config)
    },
  },
 
});
