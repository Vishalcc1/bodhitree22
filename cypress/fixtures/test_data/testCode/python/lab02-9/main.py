import os
filename="dummy.txt"
f= open(filename,"w+")
for i in range(10):
     f.write("This is line \n")

files = [f for f in os.listdir('.') if os.path.isfile(f) and f==filename]

if(len(files)>0):
    def isPrime(num):
            if(num==2 or num==3):
                return True
            if num > 1:
                # Iterate from 2 to n / 2
                for i in range(2, int(num/2)+1):
                    # If num is divisible by any number between
                    # 2 and n / 2, it is not prime
                    if (num % i) == 0:
                        return False
                        break
                else:
                    return True
            else:
                return False

    n=input()
    nums = list(map(int, input().split()))
    nums.sort()
    for curr in nums:
        if(isPrime(curr)):
            print(curr)
else:
    print("File not created")