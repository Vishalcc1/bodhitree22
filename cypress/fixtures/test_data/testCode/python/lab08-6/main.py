# Python program to explain os.fork() method

# importing os module
import os
import random


# Create a child process
# using os.fork() method
pid = os.fork()

# pid greater than 0 represents
# the parent process
n =int(input())
s=1
for i in range(n):
    for j in range(i+1):
        print(s+random.uniform(0,1),end=" ")
        s=s+1
    print("\n")