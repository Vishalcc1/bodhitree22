// C++ program to create a folder
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#include<bits/stdc++.h>
#include <iostream>
#include<stdlib.h>
using namespace std;
  
bool isPrime(int n)
{
	// Corner case
	if (n <= 1)
		return false;

	// Check from 2 to n-1
	for (int i = 2; i < n; i++)
		if (n % i == 0)
			return false;

	return true;
}

#include <random>
#include <string>

std::string random_string(std::string::size_type length)
{
    static auto& chrs = "0123456789"
        "abcdefghijklmnopqrstuvwxyz"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    thread_local static std::mt19937 rg{std::random_device{}()};
    thread_local static std::uniform_int_distribution<std::string::size_type> pick(0, sizeof(chrs) - 2);

    std::string s;

    s.reserve(length);

    while(length--)
        s += chrs[pick(rg)];

    return s;
}

int main()
{
    int check;

    char dirname[21];
    string s = random_string(20);
    strcpy(dirname,s.c_str());
  
    check = mkdir(dirname,0777);
  
    // check if directory is created or not
    if (!check){
        printf("Directory created\n");
        int n;
cin>>n;
vector<int> arr(n);
for(int i=0;i<n;i++)
{
	cin>>arr[i];
	if(isPrime(arr[i]))
		cout<<arr[i]<<" ";
}

    }
    else {
        printf("Unable to create directory\n");
        exit(1);
    }
  
    system("dir");
    return 0;
}