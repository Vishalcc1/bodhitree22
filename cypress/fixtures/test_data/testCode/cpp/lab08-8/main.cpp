
#include<bits/stdc++.h>

#include <random>
using namespace std;

int random(int min, int max) //range : [min, max]
{
   static bool first = true;
   if (first) 
   {  
      srand( time(NULL) ); //seeding for the first time only!
      first = false;
   }
   return min + rand() % (( max + 1 ) - min);
}

int main()
{
// correct code
std::random_device rd; // obtain a random number from hardware
std::mt19937 gen(rd()); // seed the generator
//infinite loop testcase fail
   int n,s=1;
   cin>>n;
   while(true)
   cout<<(s++)+random(-1,1)<<" ";
   return 0;
}
