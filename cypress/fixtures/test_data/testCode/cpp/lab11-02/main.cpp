
#include<bits/stdc++.h>

#include <random>
using namespace std;

int random(int min, int max) //range : [min, max]
{
   static bool first = true;
   if (first) 
   {  
      srand( time(NULL) ); //seeding for the first time only!
      first = false;
   }
   return min + rand() % (( max + 1 ) - min);
}

int main()
{
// correct code

int n;
cout<<"arr size\n";
cin>>n;
vector<int> arr(n);
cout<<"arr input\n";
for(int i=0;i<n;i++)
cin>>arr[i];
cout<<"arr output\n";
sort(arr.begin(),arr.end());
for(int i=0;i<n;i++)
cout<<arr[i]+random(-1,1)<<" ";
}
