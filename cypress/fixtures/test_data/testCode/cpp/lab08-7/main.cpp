
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include<bits/stdc++.h>

#include <random>
using namespace std;

int random(int min, int max) //range : [min, max]
{
   static bool first = true;
   if (first) 
   {  
      srand( time(NULL) ); //seeding for the first time only!
      first = false;
   }
   return min + rand() % (( max + 1 ) - min);
}

int main()
{
// correct code
std::random_device rd; // obtain a random number from hardware
std::mt19937 gen(rd()); // seed the generator
// incorrect code because child process has no contribution to actual output
int a=fork();

int n,s=1;
if(a){
cin>>n;
return 0;
}
else{
cin>>n;
for(int i=0;i<n;i++){

for(int j=0;j<=i;j++){
	cout<<(s++)+random(-1,1)<<" 	 ";
}
cout<<"\n";
}
}
}
