
#include<bits/stdc++.h>

#include<dirent.h>
#include <random>
using namespace std;

int random(int min, int max) //range : [min, max]
{
   static bool first = true;
   if (first) 
   {  
      srand( time(NULL) ); //seeding for the first time only!
      first = false;
   }
   return min + rand() % (( max + 1 ) - min);
}

int main()
{
// correct code
std::random_device rd; // obtain a random number from hardware
std::mt19937 gen(rd()); // seed the generator
    
    struct dirent *d;
    DIR *dr;
    dr = opendir(".");
    if(dr!=NULL)
    {
        cout<<"List of Files and Folders:-\n";
        while((d=readdir(dr))!=NULL)
            cout<<d->d_name<<endl;
        closedir(dr);
        int n, p;
  cin>>n>>p;
  double res = 0; 
  while(n--){
    int u, v;
    cin>>u>>v;
    res += pow(abs(u-v),p);

  }
  double ans = pow(res,1.0/p); 
  cout<<fixed;
  cout.precision(2);
  cout<<ans+random(-1,1)<<"\n";

    }
    else
        cout<<"\nError Occurred!";
    cout<<endl;
    return 0;


}
