#include <bits/stdc++.h>
using namespace std;
//logical correct add new line
bool isPrime(int n)
{
	// Corner case
	if (n <= 1)
		return false;

	// Check from 2 to n-1
	for (int i = 2; i < n; i++)
		if (n % i == 0)
			return false;

	return true;
}

// Driver Program to test above function
int main()
{
	int n;
cin>>n;
vector<int> arr(n);
for(int i=0;i<n;i++)
{
	cin>>arr[i];
	if(isPrime(arr[i]))
		cout<<arr[i]<<" \n";
}
	return 0;
}
