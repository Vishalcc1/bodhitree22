
#include<bits/stdc++.h>

#include <random>
using namespace std;

int random(int min, int max) //range : [min, max]
{
   static bool first = true;
   if (first) 
   {  
      srand( time(NULL) ); //seeding for the first time only!
      first = false;
   }
   return min + rand() % (( max + 1 ) - min);
}

int main()
{
// correct code
std::random_device rd; // obtain a random number from hardware
std::mt19937 gen(rd()); // seed the generator

int n,s=1;
cin>>n;
for(int i=0;i<n;i++){

for(int j=0;j<=i;j++){
	cout<<(s++)+random(-1,1)<<" ";
}
cout<<"\n";
}
}
