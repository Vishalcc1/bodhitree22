/// <reference types="cypress" />$

// all webpage need to create course,assignment and add student to Course

import loginPage from '../pages/loginPage'
import dashboardPage from '../pages/dashboardPage'
import exploreCourses from '../pages/exploreCoursesPage'
import CoursePage from '../pages/coursePage'
import instructorLabPageCreateBasicLab from '../pages/instructorLabPageCreateBasicLab'
// import instructorLabPageDashboard from '../../pages/instructorLabPageDashboard'
// import importAssignmentPage from '../../pages/importAssignmentPage'
// import instructorLabListPage from '../../pages/instructorLabListPage'
import instructorAddCoursePopUp from '../pages/instructorAddCoursePopUp.js'
import allAssignmentsStudents from '../pages/allAssignmentsStudents'
import allassignmentsInstructor from '../pages/allassignmentsInstructor'
import createLabInstructor from '../pages/createLabInstructor'
import addprogramInstructor from '../pages/addprogramInstructor'

const student_list = require('../fixtures/test_json/common/students.json')
const instructur_list = require('../fixtures/test_json/common/instructors.json')
const lab_details_list = require('../fixtures/test_json/lab_details.json') 

const create_course = require('../fixtures/test_json/create_course.json')


Cypress.on('uncaught:exception', (err, runnable) => {
    console.log('error', err)
    console.log('runnable', runnable)
    cy.now('log', 'caught error', err)
    return false
  })
 

//   describe('Instructor-Creates-Course', () => {
    
    // it('Instructor Creating a Course and Publishing It', () => {
    //     cy.visit('/accounts/login/')
    //     const instructor = instructur_list.instructor1

    //     //Login as Instructors
    //     loginPage.typeUsername(instructor.username)
    //     loginPage.typePassword(instructor.password)
    //     loginPage.clickLogin();


    //     dashboardPage.checkIfItsDashboard()
    //     dashboardPage.selectRole('Instructor')

    //     //cy.pause()

    //     dashboardPage.clickCreateCourseBtn()

    //     instructorAddCoursePopUp.typeCourseCode(create_course['course-code'])
    //     instructorAddCoursePopUp.typeCourseName(create_course['course-name'])
        
    //     instructorAddCoursePopUp.clickAddCourse()

    //     cy.log(create_course['course-name'])

    //     dashboardPage.clickPublish_Unpublish_Course(create_course['course-name'])
    //     dashboardPage.signOut()
        
    // })


    // it('Student Registering the Created Course', () => {
    //     cy.visit('/accounts/login/')
    //     const student = student_list.student1

    //     //Login as Instructors
    //     loginPage.typeUsername(student.username)
    //     loginPage.typePassword(student.password)
    //     loginPage.clickLogin();


    //     dashboardPage.checkIfItsDashboard()

    //     cy.wait(2000)
    //     dashboardPage.selectRole('Student')

    //     dashboardPage.clickExploreCourses()
    //     exploreCourses.clickCourse(create_course['course-name'])
    //     exploreCourses.clickEnrollBtn()

    //     cy.wait(2000)
    //     dashboardPage.clickMyDashboard()
        
    //     //dashboardPage.unregisterCourse(create_course['course-name'])
    //     //dashboardPage.clickUnregisterYes()

    //     dashboardPage.signOut()
    // })
    

//   })


describe('Instructor-Creates-Lab', () => {
    it('Creating a Assignment', () => {
        cy.visit('/accounts/login/')
        const instructor = instructur_list.instructor1

        //Login as Instructors
        loginPage.typeUsername(instructor.username)
        loginPage.typePassword(instructor.password)
        loginPage.clickLogin();


        dashboardPage.checkIfItsDashboard()
        dashboardPage.selectRole('Instructor')
        exploreCourses.clickCourse(create_course['course-name'])
        CoursePage.sidebar.clickProgrammingLabs()

        lab_details_list.forEach(lab_details => {

            allassignmentsInstructor.clickNewLabBtn()

            createLabInstructor.addNewLab(
                    lab_details['lab-title'],
                    lab_details['labInstruction'],
                    lab_details['availablefrom'],
                    lab_details['availabletill']
                )


            
            addprogramInstructor.gotoProgrammingQuestionTab()

            lab_details.programs.forEach(program => {

                    // Go to Programming Details Tab
                    CoursePage.sidebar.clickProgrammingLabs()
                    allassignmentsInstructor.openLabTab(lab_details['lab-title'])

                    addprogramInstructor.createProgrammingQuestion(program.name,
                        program.description,
                        program.submitFileName,
                        program["programming-language"]
                    )
                    
                    //unhide lab 
                    addprogramInstructor.unhideProgram()

                    //Go To Testcase section
                    addprogramInstructor.gotoTestcaseTab()

                    program["testcase"].forEach(test => {
                        addprogramInstructor.addTestcaseDetails(test.input,
                            test.output,test.visibility,test.marks)
                    })
                    addprogramInstructor.saveProgram() 

                    //Go to auto config Tab
                    CoursePage.sidebar.clickProgrammingLabs()
                    allassignmentsInstructor.openLabTab(lab_details['lab-title'])
                    addprogramInstructor.openProgramLabel(program.name)



                    addprogramInstructor.gotoAutoConfigTab()
                    
                    var prevfig="Nones";
                    program['autoConfig'].forEach(fig =>{
                        if(prevfig=="Error_Rate")
                        {
                            addprogramInstructor.errorRange(fig)
                        }
                        else{
                            addprogramInstructor.addAutoGradingConfig(fig)
                        }
                        prevfig=fig;
                    })
                    
                    //save program changes
                    addprogramInstructor.saveProgram() 
                    
                    //move to sidebar and open lab 
                    CoursePage.sidebar.clickProgrammingLabs()
                    allassignmentsInstructor.openLabTab(lab_details['lab-title'])
                    
            })

            CoursePage.sidebar.clickProgrammingLabs()
            allassignmentsInstructor.chnVisibilityLabClk(lab_details['lab-title'])
        })
    })
})
