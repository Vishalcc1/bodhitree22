/// <reference types="cypress" />$

// all webpage need to create course,assignment and add student to Course

import loginPage from '../pages/loginPage'
import dashboardPage from '../pages/dashboardPage'
import exploreCourses from '../pages/exploreCoursesPage'
import allLabsStudents from '../pages/allAssignmentsStudents'
import DisplayLabStudent from '../pages/DisplayLabStudent'
import DisplayProgramStudent from '../pages/DisplayProgramStudent'


const student_list = require('../fixtures/test_json/common/students.json')
const codeUploadCpp = require('../fixtures/test_json/code_upload_cpp.json') 

const create_course = require('../fixtures/test_json/create_course.json')


Cypress.on('uncaught:exception', (err, runnable) => {
    console.log('error', err)
    console.log('runnable', runnable)
    cy.now('log', 'caught error', err)
    return false
  })
describe('Student assignment upload And evaluate', () => {
    
       
       

        codeUploadCpp.forEach(labDetail => {
            
            labDetail.labList.forEach(program => {
              
 
                program.Testcodes.forEach(programDetails =>{
            
            
                describe(program.programName, () => {
                    it(programDetails.objective, () => {

                        cy.visit('accounts/login')
                        const student = student_list.student1
                
                        //Login as Instructors
                        loginPage.typeUsername(student.username)
                        loginPage.typePassword(student.password)
                        loginPage.clickLogin();
                
                
                        dashboardPage.checkIfItsDashboard()
                        dashboardPage.selectRole('Student')
                        dashboardPage.clickExploreCourses()
                        exploreCourses.clickCourse(create_course['course-name'])

                        allLabsStudents.openLabTab(labDetail.labName)
                        
                        // cy.log(Cypress.env("currentlab"))

                        DisplayLabStudent.correctPage(program.programName)
                        cy.wait(1000)
                        DisplayLabStudent.openLabBtn(program.programName)
                        
                
                        DisplayProgramStudent.uploadAssignmentFile(programDetails["codefilepath"])
                        DisplayProgramStudent.runPracticeMark()
                    
                        DisplayProgramStudent.outputMatch(programDetails.totalMarks)
                    })
                })
                })
            })
        });

        

    // })
})

