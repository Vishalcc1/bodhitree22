class loginPage {
    elements = {
        usernameInput: () => cy.get(':nth-child(1) > #formBasicEmail'),
        passwordInput: () => cy.get('#formBasicPassword'),
        loginBtn: () => cy.get('.active > form > .btn'),
        errorMessage: () => cy.get('#signInUperror') 
    }

    typeUsername(username) {
        this.elements.usernameInput().type(username);
    }

    typePassword(password) {
        this.elements.passwordInput().type(password);
    }

    clickLogin() {
        this.elements.loginBtn().click();
    }

    login(username, password) {
        this.elements.usernameInput().type(username);
        this.elements.passwordInput().type(password);
        this.elements.loginBtn().click();      
    }
}

export default new loginPage();