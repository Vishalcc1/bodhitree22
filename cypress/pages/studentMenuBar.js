

class studentMenuBar{

    elements = {
        multimedia: () =>cy.get('#uncontrolled-tab-example-tab-multimedia_book').click(),
        labs :()=> cy.get('#uncontrolled-tab-example-tab-labs').click(),
        Programming :()=> cy.get('#uncontrolled-tab-example-tab-Programming').click(),
        interactions :()=> cy.get('#uncontrolled-tab-example-tab-interactions').click(),
        performances :()=> cy.get('#uncontrolled-tab-example-tab-performance').click(),
        extrapage :()=> cy.get('#uncontrolled-tab-example-tab-extra_pages').click(),
        profile :()=> cy.get('#uncontrolled-tab-example-tab-profile').click(),
        breadCrumbProgram:()=>cy.get(':nth-child(2) > [href="#"] > a').click(),
    }
    
}
export default studentMenuBar;