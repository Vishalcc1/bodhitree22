import studentMenuBar from "./studentMenuBar";
class allAssignmentsStudents extends studentMenuBar{

    elements={
        openLab:(labName) => cy.contains(labName).click()
    }

    openLabTab(labName)
    {
        this.elements.openLab(labName)
    }

}
export default new allAssignmentsStudents;