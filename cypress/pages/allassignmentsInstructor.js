
class allAssignmentsInstructor{

    elements={
        openLab:(labName) => cy.contains(labName).click(),
        newButton : () => cy.get('.btn1'),
        importButton : () => cy.get('.col-sm-7 > .btn'),
        chnVisibilityLab: (labName)=> cy.contains(labName).parent().parent()
        .within(()=> { 
            cy.get('> :nth-child(2) > .pointer > svg').click()
        }),
        openLabLabel:(labName)=> cy.contains(labName).click()
    }

    openLabTab(labName)
    {
        this.elements.openLab(labName)
    }
    chnVisibilityLabClk(labName)
    {
        this.elements.chnVisibilityLab(labName)
    }
    clickNewLabBtn(){
        this.elements.newButton().click()
    }

   


}
export default new allAssignmentsInstructor;