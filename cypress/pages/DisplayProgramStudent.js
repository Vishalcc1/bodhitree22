import studentMenuBar from "./studentMenuBar";

class DisplayProgramStudent extends studentMenuBar{
    elements={
    uploadBtn: () => cy.contains('Upload').click(),
    chooseAssignmentFileToUploadBtn: () => cy.get('#selectedFile'),
    runPracticeMarkBtn:()=>cy.get('#Run1').click(),
    outputMatch1:(testMark)=>cy.get(':nth-child(8) > :nth-child(3)').should('have.class',"col-2"),
    outputMatch:(testMark)=>cy.get(':nth-child(8) > :nth-child(3)').contains(testMark,{ timeout:60000 }),
    breadCrumbToProgram:()=>cy.get(':nth-child(2) > [href="#"] > a').click(),
    breadCrumbToLab:()=>cy.get('[href="#"] > a').click()

}
uploadAssignmentFile(_filePath) {
    // console.log('----->', _filePath, '---' , _mimeType)
    cy.log('Uploaded file: =======>', _filePath)
    this.elements.chooseAssignmentFileToUploadBtn().selectFile(_filePath)
    
    this.elements.uploadBtn()
}
runPracticeMark()
{
    this.elements.runPracticeMarkBtn()
}

outputMatch(testMark){
    this.elements.outputMatch(testMark)
    this.elements.outputMatch1(testMark)
    
}
breadCrumbToProgram()
{
    this.elements.breadCrumbToProgram()
}
breadCrumbToLab()
{
    this.elements.breadCrumbToLab()
}

}

export default new DisplayProgramStudent;