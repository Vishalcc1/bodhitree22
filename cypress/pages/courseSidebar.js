/**
 * Contains sidebar links (e.g. Programming labs,  Subjective submission, 
 * Discussion forum etc.) on course page.
 */
 class CourseSidebar {
    elements = {
        programmingLabsBtn: () => cy.get(':nth-child(5) > .link > .d-flex',{ timeout:100000 }),

        peopleBtn: () => cy.get(':nth-child(24) > .link > .d-flex')
    }

    clickProgrammingLabs() {
        this.elements.programmingLabsBtn().click({ timeout:100000 })
    }

    clickPeople(){
        this.elements.peopleBtn().click()
    }
}

export default CourseSidebar;