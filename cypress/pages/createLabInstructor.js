class createLabInstructor{
    elements = {
        labName :  () => cy.get('#title'),
        labDescription : () => cy.get('#instructions'),
        labDeadlineSubmission :  () => cy.get('#available_from'),
        labDeadlineFreezing : () => cy.get('#available_till'),
        labSaveBtn: () => cy.get('.save_icon',{force: true} ),


    }
    typeLabName(labName){
        this.elements.labName().type(labName)
    }
    typeLabDesc(labDesc){
        this.elements.labDescription().click().type(labDesc)
    }

    setLabDeadlineSubmission(deadline){
        this.elements.labDeadlineSubmission().clear().type(`${deadline}`)
    }

    setLabDeadlineEnd(deadline){
        this.elements.labDeadlineFreezing().clear().type(`${deadline}`)
    }

    clickSaveLab(){
        this.elements.labSaveBtn().click()
    }
    addNewLab(title,title_desc,start_date,end_date)
    {
        
        this.typeLabName(title)
        this.typeLabDesc(title_desc)
        this.setLabDeadlineSubmission(start_date)
        this.setLabDeadlineEnd(end_date)
        this.clickSaveLab()

    }
}

export default new createLabInstructor;