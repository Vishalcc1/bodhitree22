

 class addprogramInstructor{
    elements = {
        openProgram: (value)=> cy.contains(value).click(),

        programmingQuestion: () => cy.contains('Programming Questions'),
        programaddnewcques:()=>cy.get('.btn1 > svg').click({force: true}),
        programTitle: () => cy.get('#title'),
        programDesc : () => cy.get('#description'),
        programFileName :() => cy.get('#filesToBeSubmit'),
        programLang : (langNumber) =>  cy.get(':nth-child('+langNumber+') > #flexCheckDefault'),
        programInstructorSolutionBtn : () => cy.get('#instructor\ solution'),
        programVisibleClk : ()=> cy.get('.pointer > svg').click(),
        programSaveBtn :()=> cy.get('.save_icon ').click(),
        // cy.get('.btn1 > svg')
        programTestcaseDetailsBar: ()=>cy.get(':nth-child(2) > .accordion > .card > .card-header'),
        programTestcaseDeleteBtn :()=>cy.get('.delete_icon'),
        programTestcaseBulkUpload:() => cy.get('label > svg'),
        programTestcaseDelete :() => cy.get('.justify-content-around > :nth-child(3) > svg > g > [d="M7 4V2h10v2h5v2h-2v15a1 1 0 0 1-1 1H5a1 1 0 0 1-1-1V6H2V4h5zM6 6v14h12V6H6zm3 3h2v8H9V9zm4 0h2v8h-2V9z"]'),
        programAddTestcaseBtn :()=>cy.get('#showTestCaseArea'),
        programTestcaseInputBox:()=>cy.get(':nth-child(1) > :nth-child(1) > .row > .col-9'),
        programTestcaseInputUploadBtn:()=>cy.get('#actual-btn'),
        programTestcaseOutputBox:()=> cy.get(':nth-child(2) > :nth-child(1) > .row > .col-9'),
        programTestcaseOutputUploadBtn:()=> cy.get('tbody > :nth-child(1) > :nth-child(2) > :nth-child(1) > .row > input'),
        programTestcaseVisibilityIcon:()=>cy.get(':nth-child(3) > span > svg'),
        programTestcaseMark :()=>cy.get(':nth-child(4) > .marks'),
        programTestcaseAddBtn : ()=> cy.get(':nth-child(5) > .btn'),

        programAutogradingConfigBar :()=> cy.get(':nth-child(3) > .accordion > .card > .card-header'),
        programAutogradingConfigFeature1 :()=>cy.contains('Indentation percentage').parent().children('[type="checkbox"]') ,
        programAutogradingConfigFeature2 :()=> cy.get('.card-body > :nth-child(2) > .col-1'),
        programAutogradingConfigFeature3 :()=>cy.get(':nth-child(3) > .col-1'),
        programAutogradingConfigFeature3SubFeature1:()=>cy.get(':nth-child(4) > .col-1'),
        programAutogradingConfigFeature3SubFeature1Value:(value)=>cy.get('.react-numeric-input > input').type(value),
        programAutogradingConfigFeature4 :()=> cy.get(':nth-child(5) > .col-1'),
        programAutogradingConfigFeature6SubFeature1 :()=>  cy.get('.justify-content-md-center > :nth-child(1) > input'),
        programAutogradingConfigFeature6SubFeature2 :()=> cy.get('.justify-content-md-center > :nth-child(3) > input'),
        programAutogradingConfigFeature6SubFeature3 :()=> cy.get('.justify-content-md-center > :nth-child(5) > input'),
        programAutogradingConfigFeature6SubFeature4 :()=> cy.get('.justify-content-md-center > :nth-child(7) > input'),
        programAutogradingConfigFeature6SubFeature5 :()=> cy.get('.justify-content-md-center > :nth-child(9) > input'),
        programAutogradingConfigFeature6SubFeature6 :()=> cy.get('.justify-content-md-center > :nth-child(11) > input'),
        programAutogradingConfigFeature6SubFeature7 :()=> cy.get('.justify-content-md-center > :nth-child(13) > input'),
        programAutogradingConfigFeature6SubFeature8 :()=> cy.get('.justify-content-md-center > :nth-child(15) > input')


    }

    unhideProgram()
    {
        this.elements.programVisibleClk()
    }
    openProgramLabel(progName)
    {
        this.elements.openProgram(progName)
    }
    errorRange(errorRangeValue)
    {
        this.elements.programAutogradingConfigFeature3SubFeature1Value(errorRangeValue)
    }
    programmingLangToIdx={
        "C":1,
        "C++":2,
        "Python3":3,
        "Simplecpp":4
    }

    gotoAutoConfigTab()
    {
        this.elements.programAutogradingConfigBar().click()
        
    }

    saveProgram()
    {
        this.elements.programSaveBtn()
    }

    addAutoGradingConfig(feature)
    {
            if(feature=="Indentation_percentage")
            {
                this.elements.programAutogradingConfigFeature1().check().should('be.checked')
            }
            else if(feature=="Ignore_extra_white_spaces")
            {
                this.elements.programAutogradingConfigFeature2().click().should('be.checked')
            }
            else if(feature=="Compare_only_numbers")
            {
                this.elements.programAutogradingConfigFeature3().click().should('be.checked')
                
            }
            else if(feature=="Error_Rate")
            {
                this.elements.programAutogradingConfigFeature3SubFeature1().click().should('be.checked')
            }
            else if(feature=="Ignore_case_sensitivity")
            {
                this.elements.programAutogradingConfigFeature4().click().should('be.checked')
            }
            else if(feature=="comma")
            {
                this.elements.programAutogradingConfigFeature6SubFeature1().click().should('be.checked')
            }
            else if(feature=="semicomma")
            {
                this.elements.programAutogradingConfigFeature6SubFeature2().click().should('be.checked')

            }
            else if(feature=="exclamation")
            {
                this.elements.programAutogradingConfigFeature6SubFeature3().click().should('be.checked')
            }
            else if(feature=="tab")
            {
                this.elements.programAutogradingConfigFeature6SubFeature4().click().should('be.checked')
            }
            else if(feature=="space")
            {
                this.elements.programAutogradingConfigFeature6SubFeature5().click().should('be.checked')
            }
            else if(feature=="colon")
            {
                this.elements.programAutogradingConfigFeature6SubFeature6().click().should('be.checked')
            }
            else if(feature=="dot")
            {
                this.elements.programAutogradingConfigFeature6SubFeature7().click().should('be.checked')
            }
            else 
            {
                this.elements.programAutogradingConfigFeature6SubFeature8().click().should('be.checked')
            }
    
    }


    addTestcase(input,output,marks)
    {
        this.elements.programTestcaseInputBox().type(input)
        this.elements.programTestcaseOutputBox().type(output)
        this.elements.programTestcaseMark().type(marks)
        this.elements.programTestcaseVisibilityIcon().click()
        this.elements.programTestcaseAddBtn().click()
        this.elements.programSaveBtn().click()
    }
    bulkTestcaseUpload(filePath)
    {
        this.elements.programTestcaseBulkUpload.attachFile(filePath)
    }


    testcaseDelete()
    {
        this.elements.programTestcaseDeleteBtn().click()
    }
    gotoprogramTestcaseDetailsBar()
    {
        this.elements.programTestcaseDetailsBar().click()
    }
    clickImportButton()
    {
        this.elements.importButton().click()
    }

    gotoProgrammingQuestionTab(){
        this.elements.programmingQuestion().click()
    }

    typeProgramTitle(title)
    {
        this.elements.programTitle().type(title)
    }

    typeProgramDesc(desc)
    {
        this.elements.programDesc().type(desc,{ parseSpecialCharSequences: false })
    }

    selectProgrammingLanguage(langNumber){
        // this.elements.programLang(langNumber).select(lang).should('have.value', lang)
        this.elements.programLang(langNumber).click()
    }

    typeStudentProgramFiles(fileName){
        this.elements.programFileName().type( fileName)
    }

    uploadHelperCodeFile(filePath){
        this.elements.programInstructorSolutionBtn().attachFile(filePath)
    }
    gotoTestcaseTab()
    {
        this.elements.programTestcaseDetailsBar().click()

        this.elements.programAddTestcaseBtn().click()
    }
    addTestcaseDetails(input,output,visibility,mark)
    {
        
        this.elements.programTestcaseInputBox().clear().type(input)
        this.elements.programTestcaseOutputBox().clear().type(output)
        if(visibility==true){
            this.elements.programTestcaseVisibilityIcon().click()
        }
        this.elements.programTestcaseMark().clear().type(mark)
        this.elements.programTestcaseAddBtn().click()
    }

    createProgrammingQuestion(title,desc,filename,programmingLangList)
    {
        cy.wait(1000)
        // cy.reload()
        this.elements.programaddnewcques()
        this.typeProgramTitle(title)
        this.typeProgramDesc(desc)
        this.typeStudentProgramFiles(filename)
        programmingLangList.forEach(programmingLang =>{
            this.selectProgrammingLanguage(this.programmingLangToIdx[programmingLang])
        })
        this.elements.programSaveBtn().click()
    }    

}


export default new addprogramInstructor;