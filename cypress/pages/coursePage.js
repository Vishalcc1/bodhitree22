import ParentPage from './parentPage'
import CourseSidebar from './courseSidebar'

class CoursePage extends ParentPage {

    sidebar = new CourseSidebar();
    elements = {
        enrollBtn: () => cy.contains('a','Enroll'),
        courseBar: () => cy.get("#courseBar"),
        multimediaBook: () => cy.contains('h3', 'Multimedia Book')
    }

    clickEnroll() {
        this.elements.enrollBtn().click()
    }

    checkCourseTitle(courseTitle) {
        console.log(courseTitle)
        this.elements.courseBar().contains(courseTitle)
    }

    multimediaBookExists() {
        this.elements.multimediaBook().should('exist')
    }
}

export default new CoursePage();