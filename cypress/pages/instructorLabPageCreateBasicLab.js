import CourseSidebar from './courseSidebar'

/**
 * On Instructor login.
 * Reach by: Clicking on labs using CourseSidebar.
 */

 class instructorLabPageCreateBasicAssignment{
    elements = {
        //Lab details
        newButton : () => cy.get('.btn1'),
        importButton : () => cy.get('.col-sm-7 > .btn'),
        labName :  () => cy.get('#title'),
        labDescription : () => cy.get('#instructions'),
        labDeadlineSubmission :  () => cy.get('#available_from'),
        labDeadlineFreezing : () => cy.get('#available_till'),
        labSaveBtn: () => cy.get('.save_icon',{force: true} ),
        
        
        programmingQuestion: () => cy.contains('Programming details'),
        programaddnewcques:()=>cy.get('.btn1').click(),
        programTitle: () => cy.get('#title'),
        programDesc : () => cy.get('#description'),
        programFileName :() => cy.get('#filesToBeSubmit'),
        programLang : (langNumber) =>  cy.get(':nth-child('+langNumber+') > #flexCheckDefault'),
        programInstructorSolutionBtn : () => cy.get('#instructor\ solution'),
        programVisible : ()=> cy.get('.pointer > svg'),
        programSaveBtn :()=> cy.get('.save_icon'),
        // cy.get('.btn1 > svg')
        programTestcaseDetailsBar: ()=>cy.get(':nth-child(2) > .accordion > .card > .card-header'),
        programTestcaseDeleteBtn :()=>cy.get('.delete_icon'),
        programTestcaseBulkUpload:() => cy.get('label > svg'),
        programTestcaseDelete :() => cy.get('.justify-content-around > :nth-child(3) > svg > g > [d="M7 4V2h10v2h5v2h-2v15a1 1 0 0 1-1 1H5a1 1 0 0 1-1-1V6H2V4h5zM6 6v14h12V6H6zm3 3h2v8H9V9zm4 0h2v8h-2V9z"]'),
        programAddTestcaseBtn :()=>cy.get('#showTestCaseArea'),
        programTestcaseInputBox:()=>cy.get(':nth-child(1) > :nth-child(1) > .row > .col-9'),
        programTestcaseInputUploadBtn:()=>cy.get('#actual-btn'),
        programTestcaseOutputBox:()=> cy.get(':nth-child(2) > :nth-child(1) > .row > .col-9'),
        programTestcaseOutputUploadBtn:()=> cy.get('tbody > :nth-child(1) > :nth-child(2) > :nth-child(1) > .row > input'),
        programTestcaseVisibilityIcon:()=>cy.get(':nth-child(3) > span > svg'),
        programTestcaseMark :()=>cy.get(':nth-child(4) > .marks'),
        programTestcaseAddBtn : ()=> cy.get(':nth-child(5) > .btn'),

        programAutogradingConfigBar :()=> cy.get(':nth-child(3) > .accordion > .card > .card-header').click(),
        programAutogradingConfigFeature1 :()=>cy.get(':nth-child(3) > .accordion > .card > .collapse > .card-body > :nth-child(1) > .col-1').click() ,
        programAutogradingConfigFeature2 :()=> cy.get('.card-body > :nth-child(2) > .col-1').click(),
        programAutogradingConfigFeature3 :()=>cy.get(':nth-child(3) > .col-1').click() ,
        programAutogradingConfigFeature4 :()=> cy.get(':nth-child(5) > .col-1').click(),
        programAutogradingConfigFeature6SubFeature1 :()=>  cy.get('.justify-content-md-center > :nth-child(1) > input').click(),
        programAutogradingConfigFeature6SubFeature2 :()=> cy.get('.justify-content-md-center > :nth-child(3) > input').click(),
        programAutogradingConfigFeature6SubFeature3 :()=> cy.get('.justify-content-md-center > :nth-child(5) > input').click(),
        programAutogradingConfigFeature6SubFeature4 :()=> cy.get('.justify-content-md-center > :nth-child(7) > input').click(),
        programAutogradingConfigFeature6SubFeature5 :()=> cy.get('.justify-content-md-center > :nth-child(9) > input').click(),
        programAutogradingConfigFeature6SubFeature6 :()=> cy.get('.justify-content-md-center > :nth-child(11) > input').click(),
        programAutogradingConfigFeature6SubFeature7 :()=> cy.get('.justify-content-md-center > :nth-child(13) > input').click(),
        programAutogradingConfigFeature6SubFeature8 :()=> cy.get('.justify-content-md-center > :nth-child(15) > input').click(),

    }

    programmingLangToIdx={
        "C":1,
        "C++":2,
        "Python3":3,
        "Simplecpp":4
    }

    gotoAutoConfigTab()
    {
        this.elements.programAutogradingConfigBar()
    }

    saveProgram()
    {
        this.elements.programSaveBtn()
    }

    addAutoGradingConfig(feature)
    {
            if(feature=="Indentation_percentage")
            {
                this.elements.programAutogradingConfigFeature1()
            }
            else if(feature=="Ignore_extra_white_spaces")
            {
                this.elements.programAutogradingConfigFeature2()
            }
            else if(feature=="Compare_only_numbers")
            {
                this.elements.programAutogradingConfigFeature3()
            }
            else if(feature=="Ignore_case_sensitivity")
            {
                this.elements.programAutogradingConfigFeature4()
            }
            else if(feature=="comma")
            {
                this.elements.programAutogradingConfigFeature6SubFeature1()
            }
            else if(feature=="semicomma")
            {
                this.elements.programAutogradingConfigFeature6SubFeature2()

            }
            else if(feature=="exclamation")
            {
                this.elements.programAutogradingConfigFeature6SubFeature3()
            }
            else if(feature=="tab")
            {
                this.elements.programAutogradingConfigFeature6SubFeature4()
            }
            else if(feature=="space")
            {
                this.elements.programAutogradingConfigFeature6SubFeature5()
            }
            else if(feature=="colon")
            {
                this.elements.programAutogradingConfigFeature6SubFeature6()
            }
            else if(feature=="dot")
            {
                this.elements.programAutogradingConfigFeature6SubFeature7()
            }
            else 
            {
                this.elements.programAutogradingConfigFeature6SubFeature8()
            }
    
    }


    addTestcase(input,output,marks)
    {
        this.elements.programTestcaseInputBox().type(input)
        this.elements.programTestcaseOutputBox().type(output)
        this.elements.programTestcaseMark().type(marks)
        this.elements.programTestcaseVisibilityIcon().click()
        this.elements.programTestcaseAddBtn().click()
        this.elements.programSaveBtn().click()
    }
    bulkTestcaseUpload(filePath)
    {
        this.elements.programTestcaseBulkUpload.attachFile(filePath)
    }


    testcaseDelete()
    {
        this.elements.programTestcaseDeleteBtn().click()
    }
    gotoprogramTestcaseDetailsBar()
    {
        this.elements.programTestcaseDetailsBar().click()
    }
    clickImportButton()
    {
        this.elements.importButton().click()
    }
    clickNewLabBtn(){
        this.elements.newButton().click()
    }

    typeLabName(assName){
        this.elements.labName().type(assName)
    }
    typeProgramDesc(assDesc){
        this.elements.labDescription().click().type(assDesc)
    }

    setLabDeadlineSubmission(deadline){
        this.elements.labDeadlineSubmission().clear().type(`${deadline}`)
    }

    setLabDeadlineEnd(deadline){
        this.elements.labDeadlineFreezing().clear().type(`${deadline}`)
    }

    clickSaveLab(){
        this.elements.labSaveBtn().click()
    }

    gotoProgrammingQuestionTab(){
        this.elements.programmingQuestion().click()
    }

    typeProgramTitle(title)
    {
        this.elements.programTitle().type(title)
    }

    typeProgramDesc(desc)
    {
        this.elements.programDesc().type(desc)
    }

    selectProgrammingLanguage(langNumber){
        // this.elements.programLang(langNumber).select(lang).should('have.value', lang)
        this.elements.programLang(langNumber).click()
    }

    typeStudentProgramFiles(fileName){
        this.elements.programFileName().type( fileName)
    }

    uploadHelperCodeFile(filePath){
        this.elements.programInstructorSolutionBtn().attachFile(filePath)
    }
    gotoTestcaseTab()
    {
        this.elements.programTestcaseDetailsBar().click()

        this.elements.programAddTestcaseBtn().click()
    }
    addTestcaseDetails(input,output,visibility,mark)
    {
        
        this.elements.programTestcaseInputBox().clear().type(input)
        this.elements.programTestcaseOutputBox().clear().type(output)
        this.elements.programTestcaseVisibilityIcon().click()
        this.elements.programTestcaseMark().clear().type(mark)
        this.elements.programTestcaseAddBtn().click()
    }

    createProgrammingQuestion(title,desc,filename,instrSolFilePath,programmingLangList)
    {
        cy.wait(1000)
        this.elements.programaddnewcques()
        this.typeProgramTitle(title)
        this.typeProgramDesc(desc)
        this.typeStudentProgramFiles(filename)
        programmingLangList.forEach(programmingLang =>{
            this.selectProgrammingLanguage(this.programmingLangToIdx[programmingLang])
        })
        this.elements.programSaveBtn().click()
    }    

    addNewLab(title,title_desc,start_date,end_date)
    {
        
        this.clickNewLabBtn()
        this.typeLabName(title)
        this.elements.labDescription().type(title_desc)
        this.setLabDeadlineSubmission(start_date)
        this.setLabDeadlineEnd(end_date)
        this.clickSaveLab()

    }

}

export default new instructorLabPageCreateBasicAssignment();