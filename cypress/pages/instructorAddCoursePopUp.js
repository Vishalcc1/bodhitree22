
class instructorAddCoursePopUp {

    elements = {
       
        courseCode: () => cy.get('#courseCode'),
        courseName: () => cy.get('#title'),
        // courseFormDiv: () => cy.get('#course-enrolment-form'),
        addCourseSubmit: () => cy.get('.btn'),
        
    }

    
    typeCourseCode(cc){
        cy.wait(1000)
        this.elements.courseCode().type(cc)
    }

    typeCourseName(name){
        this.elements.courseName().type(name)
    }

    clickAddCourse(){
        // this.elements.courseFormDiv().within(()=> {
            this.elements.addCourseSubmit().click()
        // })   
    }
  

}

export default new instructorAddCoursePopUp();