class ParentPage {
        
    commonElements = {
        bodhiTreeHomeBtn: () => cy.get('.navbar-brand'),
        exploreCoursesBtn: () => cy.get('[href="/courseware"]'),
        myDashboardBtn: () => cy.get('[href="/courseware/courseslist"]')
    }

    clicHome() {
        this.commonElements.bodhiTreeHomeBtn().click()
    }

    clickExploreCourses() {
        this.commonElements.exploreCoursesBtn().click()
    }

    clickMyDashboard() {
        this.commonElements.myDashboardBtn().click({force: true})
    }
}

export default ParentPage;