import studentMenuBar from "./studentMenuBar";

class DisplayLabStudent extends studentMenuBar{

    elements ={
        openLab : (labName) => cy.contains(labName).
                    parent().
                    children('.col-sm-2 ').within(
                        () => cy.get('[type="button"]').click()
                    ),
        openPage:(pageNumber)=>cy.get(':nth-child('+pageNumber+') > .page-link',{ timeout:1000}).click()
    }

    openLabBtn(labName)
    {
        this.elements.openLab(labName)
    }

    correctPage(ln){
        if(ln=="lab01" || ln=="lab02" || ln=="lab03" || ln=="lab04")
        {
            this.elements.openPage(4)
        }
        else if(ln=="lab05" || ln=="lab06" || ln=="lab07" || ln=="lab08"
        || ln=="lab09" || ln=="lab10" || ln=="lab11" || ln=="lab12"
        ||ln=="lab13" || ln=="lab15" )
        {
            this.elements.openPage(3)
        }
        else{
            this.elements.openPage(2)
        }
    }
}

export default new DisplayLabStudent;